# packer-scientific

[Packer](https://www.packer.io/) template for Scientific Linux 6.6

[https://atlas.hashicorp.com/joshbeard/boxes/scientific-6.6-64](https://atlas.hashicorp.com/joshbeard/boxes/scientific-6.6-64)
